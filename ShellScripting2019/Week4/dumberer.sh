#!/bin/bash
#Script that finds the highest temperature registered in November 2011

resFile=/tmp/Week4Ex4-3/max-temps-2011-11.txt #Path for the final results
rm $resFile #To ensure that there's an empty file where to place the info

for DAY in /tmp/Week4Ex4-3/lost24/monitor/2011.11* #For every day in the month
do
	highestTemp=-273 #Initialize the value at absolute zero
	prefix=${DAY%.*} #The prefix to eliminate from the path to be able to get the file name
	currDay=$DAY
	currDay=${currDay#$prefix}
	currDay=${currDay#.} #Day of the month just as a number

	for LOG in $DAY/*/hp-temps.txt #For each log file
	do
      currTemp=$(sed -e 's/[[:space:]]\{1,\}/,/g' -e 's,\/,\,,g' $LOG|grep 'PROCESSOR_ZONE'|cut -d ',' -f 3) #Extract the temperature
	  currTemp=${currTemp%C} #For numerical comparison we take out the C
	  if [ $highestTemp -lt $currTemp ] ; then
	  	highestTemp=$currTemp
	  fi
	done

	echo "$currDay $highestTemp" >> $resFile #Place day, highest temperature and lowest temperature into the file
done

plotName=${resFile%.txt} #The name of the plot file
plotName=$plotName.eps

gnuplot -e "inName='$resFile'" -e "outName='$plotName'" ./maxplotter.gnuplot
