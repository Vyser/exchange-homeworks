#!/bin/bash

number=$1
reg='^[0-9] +$'

if ! [[ $number =~ $reg ]] ; then
	echo 'The input must be an integer! I shall print a shortcat'
	number=1
fi

head -n9 ./shortcat.txt

for ((i = 1; i <= number; i++))
do
	echo '  |       | '
done

tail -n6 ./shortcat.txt
