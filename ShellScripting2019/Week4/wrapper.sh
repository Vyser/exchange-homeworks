#!/bin/bash

fmt='eps'
temp='b'
trigger='no'

while getopts 'cwbah' opt; do
    case $opt in
        c)
            temp='c' >&2
            ;;
        w)
            temp='w' >&2
            ;;
        b)
            temp='b' >&2
            ;;
        a)
            fmt=dumb >&2
            ;;
        h)
            trigger=yes
            echo "Hello weary traveler!" >&2
            echo "I see you have stumbled upon my little precious script!" >&2
            echo "In short, my script does this:
            -It looks into the temperature logs of a certain device from the University of Helsinki
            -It makes me sad to say this, but as of now it can only look into the logs of November of 2011
            -It also makes some cool gtaphs according to the temperatures with the help of gnuplot! 
            It can do a bunch of stuff depending on the flags that you signal it. As they stand, the flags signal these options:
                    * The flag -b tells the script to grab the information of both the highest and lowest temperatures of each day
                of the month. This is the default for the script.
                    * The flag -c tells the script to only get the coldest temperatures of each day.
                    * The flag -w tells the script to only get the warmest temperatures of each day.
                            ****** THE SCRIPT WILL GET THE TEMPERATURES SPECIFIED BY THE LAST OF THESE FLAGS TO APPEAR ******
                    * The flag -a will make the script plot the graphs with ASCII art on your console terminal
                    * And finally, the one you invoked, -h prints this helpful message without looking for the temperatures or anything" >&2
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            ;;
    esac
done

if [ "$trigger" = "no" ] ; then
    /tmp/Week4Ex4-3/temps.sh $fmt $temp #If the user didn't ask for help, execute the script normally
fi