set datafile sep ' '
set terminal fmt #fmt holds either eps or dumb
if (fmt eq "eps"){
    set output outName #If we want ASCII art it's printed out in the terminal
}
set title "Core processor temperatures"
set xlabel "Day"
set ylabel "Temperature (C)"

if (temp eq "b"){
    plot inName using 1:2 w lp title "Maximum temperature", inName using 1:3 w lp title "Minimum temperature"
}
if (temp eq "w"){
    plot inName using 1:2 w lp title "Maximum temperature"
}
if (temp eq "c"){
    plot inName using 1:3 w lp title "Minimum temperature"
}
