set datafile sep ' '
set terminal eps
set output outName
set title "Core processor temperatures"
set xlabel "Day"
set ylabel "Temperature (C)"

plot inName using 1:2 w lp title "Maximum temperature"
