#!/bin/bash
#Script to hipstafy pics in a directory

for FILE in $1*.jpg
do
	inputfile=$FILE
	prefix=${inputfile%.jpg}
	prefix=${prefix#$1}
	outputfile=$2$prefix-hipstah.jpg
	convert -sepia-tone 60% +polaroid $inputfile $outputfile
done
