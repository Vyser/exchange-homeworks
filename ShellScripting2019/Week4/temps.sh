#!/bin/bash
#Script that finds the highest temperature registered in November 2011

resFile=/tmp/Week4Ex4-3/min-max-temps-2011-11.txt #Path for the final results
rm $resFile

for DAY in /tmp/Week4Ex4-3/lost24/monitor/2011.11* #For every day in the month
do
	highestTemp=-273 #Initialize the value at absolute zero
	lowestTemp=1000 #Initialize the value in some big impossible number (at 1000C the computer wouldn't exist anymore)
	prefix=${DAY%.*}
	currDay=$DAY
	currDay=${currDay#$prefix}
	currDay=${currDay#.}

	for LOG in $DAY/*/hp-temps.txt #For each log file
	do
		currTemp=$(sed -e 's/[[:space:]]\{1,\}/,/g' -e 's,\/,\,,g' $LOG|grep 'PROCESSOR_ZONE'|cut -d ',' -f 3) #Extract the temperature
	  currTemp=${currTemp%C} #For numerical comparison we take out the C
	  if [ $highestTemp -lt $currTemp ] ; then
	  	highestTemp=$currTemp
	  fi
	  if [ $currTemp -lt $lowestTemp ] ; then
	  	lowestTemp=$currTemp
	  fi
	done

	echo "$currDay $highestTemp $lowestTemp" >> $resFile
done

plotName=${resFile%.txt}
plotName=$plotName.eps

gnuplot -e "inName='$resFile'" -e "outName='$plotName'" -e "fmt='$1'" -e "temp='$2'" ./plotter.gnuplot
