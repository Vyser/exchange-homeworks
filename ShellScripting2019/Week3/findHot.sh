#!/bin/bash
#Script that finds the highest temperature registered in November 2011

highestTemp=-273 #Initialize the value at absolute zero
highestFile="" #The file with the highest temp is unknown

for FILE in ./lost24/monitor/2011.11.*/*/hp-temps.txt #For each log file
do
	currTemp=$(sed -e 's/[[:space:]]\{1,\}/,/g' -e 's,\/,\,,g' $FILE|grep 'PROCESSOR_ZONE'|cut -d ',' -f 3) #Extract the temperature
	currTemp=${currTemp%C} #For numerical comparison we take out the C
	if [ $highestTemp -lt $currTemp ]; then
		highestTemp=$currTemp
		highestFile=$FILE
	fi
done

echo "The highest temperature was of $highestTemp C and it is in $highestFile" #Double quotes to evaluate the variables
