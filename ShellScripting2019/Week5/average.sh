#!/bin/bash
#Script to calculate the average of the inputs

sum=0
count=0

for number in "$@" #For each input
do
	sum=$((sum + number))
	count=$((++count))
done

#The user decides the amount of decimal precision that he/she wants
read -p "Enter the amount of decimal spaces that you want for the mean: " dec

echo "scale=$dec; $sum / $count" | bc
