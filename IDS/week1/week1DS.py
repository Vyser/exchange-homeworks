import numpy as np
import scipy as sp
from sklearn.datasets import load_boston
import json
import string
from nltk.stem.porter import *

################################# Exercise 1 ###################################

#### BASICS
# a = list(range(0, int(1e7)))
# b = a

# def add_loop(a, b):
    # c = [1]*len(a)
    # for i in range(0, (len(a)-1)):
        # c[i] = a[i] + b[i]
    # return c
# 
# c = add_loop(a,b)
# print(c[10])
# print(a[int(1e7-1)])

# a = np.arange(int(1e7))
# b = a
# 
# def add_np(a,b): ## THIS METHOD IS MUCH FASTER
    # c = np.add(a,b)
    # return c
# 
# c = add_np(a,b)
# print(c[10])


#### ARRAY MANIPULATION
# A = np.arange(0,100).reshape(10,10)
# print(A)
# 
# B = np.arange(0,100.0).reshape(10,10)
# print(B)
# 
# iden10 = np.eye(10)
# ones10 = np.ones((10,10))
# 
# C = np.subtract(ones10, iden10)
# print(C)
# 
# D = np.fliplr(C)
# print(D)
# 
# detC = np.linalg.det(C)
# detD = np.linalg.det(D)
# detCD = np.linalg.det(np.matmul(C,D))
# print(detC*detD)
# print(detCD)
##


#### Slicing
x , y  = load_boston(return_X_y = True)

crimInd = np.where(x[:, 0] > 1)
print(crimInd[0][:3])

ptsr = np.where( (x[:, 10] > 16) & (x[:, 10] < 18) )
print(len(ptsr[0]))

house = np.where(y > 25)[0]
avgNit = x[house, 4].mean()
print(avgNit)


################################# Exercise 2 ###################################

with open('Automotive_5.json') as json_file:
    data = [json.loads(line) for line in json_file]
    reviews = []
    pos = []
    neg = []
    stemmer = PorterStemmer()
    text_file = open("stop-word-list.txt", "r")
    stop = text_file.readlines()
    stop_list = []
    exclude = set(string.punctuation)
    for word in stop:
        stop_list.append(word.rstrip('\n'))
    text_file.close()

    for l in data:
        text = l["reviewText"]
        read = ''
        for c in text: 
            if c not in exclude:
                read += c
        
        read = " ".join(stemmer.stem(x) for x in read.lower().split(" ") if x not in stop_list)
        if l["overall"] > 3:
            pos.append(read)
        elif l["overall"] < 3:
            neg.append(read)
            
    with open('pos_review.txt', 'w') as f:
        for item in pos:
            f.write("%s\n" % item)
    with open('neg_review.txt', 'w') as f:
        for item in neg:
            f.write("%s\n" % item)