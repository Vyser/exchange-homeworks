import pandas as pd
import numpy as np
import scipy as sp
import json
import collections
import sklearn as sk
import sklearn.feature_extraction
from sklearn.feature_extraction.text import TfidfVectorizer
import matplotlib.pyplot as plt
# from pathlib import Path

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

def ex1():
    titanic_set = pd.read_csv('train.csv', delimiter = ',').drop(columns = 'Name')
    titanic_set['Deck'] = titanic_set['Cabin'].str[0]
    contDat = {'Age', 'Fare'}
    discDat = {'Sex', 'Pclass', 'Survived', 'SibSp', 'Parch', 'Embarked', 'Deck', 'Ticket', 'Cabin'}
    for cat in titanic_set:
        if cat in contDat:
            titanic_set[cat] = titanic_set[cat].fillna(titanic_set[cat].mean())
        elif cat in discDat:
            titanic_set[cat] = titanic_set[cat].fillna(titanic_set[cat].mode()[0]).astype('category').cat.codes
            # titanic_set[cat] = titanic_set[cat].astype('category').cat.codes
    titanic_set.to_csv('./exercise1.csv')
    titanic_set.to_json('./exercise1.json', orient = 'records')

    
    print(titanic_set)

def prop(dataset, cat, val):
    total = 0.0
    per = 0.0
    for person in dataset[cat]:
        if person == val:
            per += 1
            total += 1
        else:
            total += 1
    return (per/total)*100

def plotter(dataset, cat, surv, bins):
    surv_fare = pd.Series(dataset[cat])

    surv_fare.plot.hist(grid=True, bins=bins, rwidth=0.9,
                       color='#607c8e')
    plt.title(surv + ' ' + cat)
    plt.xlabel(cat)
    plt.ylabel('Amount of people')
    plt.grid(axis='y', alpha=0.75)
    plt.show()


def ex2():
    titanic_set = pd.read_csv('exercise1.csv', delimiter = ',')
    contDat = {'Age', 'Fare'}
    discDat = {'Sex', 'Pclass', 'Survived', 'SibSp', 'Parch', 'Embarked', 'Deck'}
    # contMean = []
    # discMode = []
    avgSurv = {}
    avgnSurv = {}
    # for cat in titanic_set:
        # if cat in contDat:
            # contMean.append(titanic_set[cat].mean())
        # elif cat in discDat:
            # discMode.append(titanic_set[cat].mode())
    surv_data = titanic_set[titanic_set.Survived == 1]
    # print(surv_data.head())
    non_data = titanic_set[titanic_set.Survived == 0]
    # print(non_data)
    for cat in titanic_set:
        if cat in contDat:
            avgSurv[cat] = surv_data[cat].mean()
            avgnSurv[cat] = non_data[cat].mean()
        elif cat in discDat:
            avgSurv[cat] = surv_data[cat].mode()
            avgnSurv[cat] = non_data[cat].mode()
    print(avgnSurv)
    print(avgSurv)
    survClassProp = prop(surv_data, 'Pclass', 0)
    print(survClassProp)
    survSexProp = prop(surv_data, 'Sex', 0)
    print(survSexProp)
    plotter(surv_data, 'Fare', 'Survivor', 20)
    plotter(surv_data, 'Deck', 'Survivor', 7)
    plotter(surv_data, 'Sex', 'Survivor', 2)
    plotter(surv_data, 'SibSp', 'Survivor', 5)
    plotter(non_data, 'Fare', 'Non-Survivor', 20)
    plotter(non_data, 'Deck', 'Non-Survivor', 7)
    plotter(non_data, 'Sex', 'Non-Survivor', 2)
    plotter(non_data, 'SibSp', 'Non-Survivor', 5)
    # surv_data['Fare'].hist()
    ####################################### SURVIVORS KIND OF SIMILAR TO AVG #######################################
    ####### Passenger ID 24, 14 in calc
    ####### Passenger ID 152, line 55
    ####### Passenger ID 219, line 78
    ####### Passenger ID 310, line 116
    ####### Passenger ID 725, line 284
    ####### Passenger ID 810, line 810


def common_words(text):
    file = open(text, encoding = "utf8")
    read = file.read()
    wordcount = {}
    for word in read.split():
        if word not in wordcount:
            wordcount[word] = 1
        else:
            wordcount[word] += 1
    n_print = int(input("How many most common words to print: "))
    print("\nOK. The {} most common words are as follows\n".format(n_print))
    word_counter = collections.Counter(wordcount)
    for word, count in word_counter.most_common(n_print):
        print(word, ": ", count)
    file.close
    return read

def ex3():
    neg = common_words('neg_review.txt')
    pos = common_words('pos_review.txt')
    docs = [pos, neg]

    # settings that you use for count vectorizer will go here
    tfidf_vectorizer=TfidfVectorizer(use_idf=True)
    
    # just send in all your docs here
    tfidf_vectorizer_vectors=tfidf_vectorizer.fit_transform(docs)

    # get the first vector out (for the first document)
    first_vector_tfidfvectorizer=tfidf_vectorizer_vectors[0]
    second_vector_tfidfvectorizer=tfidf_vectorizer_vectors[1]
    # print(tfidf_vectorizer_vectors[0])
    # print('*****************************************************************')
    # print(tfidf_vectorizer_vectors[1])
    
    # place tf-idf values in a pandas data frame
    dfneg = pd.DataFrame(first_vector_tfidfvectorizer.T.todense(), index=tfidf_vectorizer.get_feature_names(), columns=["tfidfneg"])
    dfpos = pd.DataFrame(second_vector_tfidfvectorizer.T.todense(), index=tfidf_vectorizer.get_feature_names(), columns=["tfidfpos"])
    
    df = dfneg.join(dfpos)
    df.sort_values('tfidfneg', inplace = True)
    df.sort_values('tfidfpos', inplace = True)
    print(df.tail())
    mat = df.to_numpy()
    mat = mat.transpose()
    print(mat)
    print(mat.shape)
    # print(dfpos.tail())
    # vecneg = dfneg.to_numpy()
    # vecpos = dfpos.to_numpy()
    # mat = np.concatenate((vecneg, vecpos), axis=1)
    # mat = np.transpose(mat)
    # print(mat)
    # print(mat.shape)
    # print(df.to_numpy())
    # print(df.columns)
    # print(df.shape)
    


def main():
    # ex1()
    # ex2()
    ex3()

if __name__ == '__main__':
    main()
