import numpy as np
import scipy.misc
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.multiclass import OneVsRestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.dummy import DummyClassifier
from sklearn.metrics import accuracy_score

df = pd.read_csv('hasy-data-labels.csv')
df = df[df.symbol_id >= 70]
df = df[df.symbol_id <= 80]
data_vect = []
targets = []

#The image consists of an array of arrays, this is why we need to 
#transform it to a single dimension [array flattening]
for index, row in df.iterrows():
    img = scipy.misc.imread(row['path'], 1, 'L')
    img = img.flatten()
    data_vect.append(img)
    targets.append(row['latex'])

training_data, test_data, train_target, test_target = train_test_split(data_vect, targets, train_size=0.8)
print('training_data size = ', len(training_data[0]))
print('test_data size = ', len(test_data[0]))

model = LogisticRegression()
ovr = OneVsRestClassifier(model).fit(training_data, train_target)

dummy = DummyClassifier('most_frequent')
dummy.fit(training_data, train_target)

#predicting & comparing
predictedOvr = ovr.predict(test_data)
predictedDummy = dummy.predict(test_data)

dummy = DummyClassifier('most_frequent')
dummy.fit(training_data, train_target)

#predicting & comparing
predictedOvr = ovr.predict(test_data)
predictedDummy = dummy.predict(test_data)

print('Majority classifier result')
print(predictedDummy)
print('Majority classifier accuracy:', accuracy_score(test_target,predictedDummy))

print('OVR predictor result')
print(predictedOvr)
print('OVR accuracy:', accuracy_score(test_target,predictedOvr))

fig = plt.figure(9)
k = 331 #hardcoded subplot code to generate 3x3 subplots using the loop

for i in range(0, len(test_target)):
    if(test_target[i]!=predictedOvr[i]):
        im = test_data[i].reshape((32,32))
        ax = plt.subplot(k)
        ax.set_axis_off()
        ax.title.set_text("Predicted: "+predictedOvr[i]+"("+test_target[i]+")")
        plt.imshow(im)

        # again hardcoded so it breaks after we have eg 9 samples to show
        k+=1
        if(k>339):
            break

plt.tight_layout()
plt.show()
